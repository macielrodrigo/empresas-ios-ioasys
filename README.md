![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer as informações necessárias para o projeto Empresas.


### Especifiações ###

* Xcode 11.3
* Swift 5
* iOS >= 13.2

### ESCOPO DO PROJETO ###

* Login e acesso de Usuário já registrado
* Listagem de Empresas
* Detalhamento de Empresas


### Dados para Teste ###

* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234
