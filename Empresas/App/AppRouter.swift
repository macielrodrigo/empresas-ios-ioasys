//
//  AppRouter.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 17/01/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

final class AppRouter {
    
    private var window: UIWindow
    private var navigationController: UINavigationController
    
    private var appLaunchOptions: [UIApplication.LaunchOptionsKey: Any]?
    
    init(windowScene: UIWindowScene) {
        
        self.window = UIWindow(windowScene: windowScene)
        self.navigationController = UINavigationController()
        
    }
    
    func visibleViewController(_ rootViewController: UIViewController) {
        
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
    
}

extension AppRouter {
    
    func showInitialViewController() {
        
        if LoginManager.sharedInstance.hasLogin() {
            showListEnterpriseViewController()
        } else {
            showLoginViewController()
        }
    }
    
    func showLoginViewController() {
        self.visibleViewController(self.navigationController)
        
        let controller = LoginViewController(appRouter: self)
        navigationController.viewControllers = [controller]
        navigationController.navigationBar.isHidden = true
    }
    
    func showListEnterpriseViewController() {
        self.visibleViewController(self.navigationController)
        
        let controller = ListEnterpriseViewController(appRouter: self)
        controller.modalPresentationStyle = .fullScreen
        navigationController.navigationBar.isHidden = false
        navigationController.setViewControllers([controller], animated: true)
    }
    
    func showDetailsEnterpriseViewController(with enterprise: Enterprise? = nil) {
        
        let controller = EnterpriseDetailsViewController(enterprise: enterprise)
        navigationController.pushViewController(controller, animated: true)
    }
    
    func didSelectEnterprise(tableView: UITableView?, indexPath: IndexPath?, enterprise: Enterprise?) {
        
        showDetailsEnterpriseViewController(with: enterprise)
    }
    
    func logout() {
        
        UserSessionManager.sharedInstance.removeSessionHeaders(key: .headers)
        //remove stack
        self.navigationController.viewControllers.removeAll()
        self.showInitialViewController()
    }
}
