//
//  Appereance.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 12/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

struct Appereance {
    
    
    static func install() {
        
        //UIWindow.appearance().tintColor = tintColor
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().backgroundColor = .white
        UINavigationBar.appearance().barTintColor = UIColor(named: "darkish_pink")
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white, .font: UIFont.systemFont(ofSize: 15, weight: .bold)]
        let backImage = UIImage(named: "ic_back")
        let barButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UINavigationBar.appearance().topItem?.backBarButtonItem = barButton
        
        //if #available(iOS 11.0, *) {
           // UINavigationBar.appearance().prefersLargeTitles = true
        //}
    }
    
}
