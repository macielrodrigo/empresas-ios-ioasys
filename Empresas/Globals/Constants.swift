//
//  Constants.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 17/01/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

struct Constants {
    
    static let baseUrl = "https://empresas.ioasys.com.br/api/"
    static let apiVersion = "v1/"
    
}
