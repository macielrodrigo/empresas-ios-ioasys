//
//  LoginManager.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 12/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation


class LoginManager {
    
    static let sharedInstance = LoginManager()
    
    var login: Login? {
    
        set {
              if let valueToSave = newValue {
                UserDefaults.saveObjectWith(key: .login, object: valueToSave)
              } else
              {
                  removeLogin(key: .login)
              }
          }
          get {
            let login = UserDefaults.getObjectWith(key: .login, type: Login.self)
              
              return login
          }
        
    }
    
    func saveLogin(email: String?, password: String? ) {
        if let email = email, let password = password {
            
            self.login = Login(email: email, password: password)
        }
    }
    
    func removeLogin(key: keysObjects) {
          UserDefaults.removeObjectWith(key: key)
    }
    
    func hasLogin() -> Bool {

        guard let userSession = UserSessionManager.sharedInstance.userSession else {return false}
        
        if userSession.accessToken.isEmpty {
            return false
        }
        
        return true
      }
      
    
}
