//
//  Login.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

class Login: NSObject, NSCoding, Encodable {

    let email: String?
    let password: String?
    
    init(email: String, password: String) {
        self.email = email
        self.password = password

    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let email = aDecoder.decodeObject(forKey: "email") as? String,
            let password = aDecoder.decodeObject(forKey: "password") as? String
                else {
                return nil
        }
        
        self.init(email: email, password: password)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(email, forKey: "email")
        aCoder.encode(password, forKey: "password")

    }
    
}
