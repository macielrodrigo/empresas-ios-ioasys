//
//  Portfolio.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

struct Portfolio : Codable {
	let enterprises_number : Int?
	let enterprises : [String]?

	enum CodingKeys: String, CodingKey {

		case enterprises_number = "enterprises_number"
		case enterprises = "enterprises"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		enterprises_number = try values.decodeIfPresent(Int.self, forKey: .enterprises_number)
		enterprises = try values.decodeIfPresent([String].self, forKey: .enterprises)
	}

}
