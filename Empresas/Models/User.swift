//
//  User.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation
struct User : Codable {
	let investor : Investor?
	let enterprise : String?
	let success : Bool?

	enum CodingKeys: String, CodingKey {

		case investor = "investor"
		case enterprise = "enterprise"
		case success = "success"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		investor = try values.decodeIfPresent(Investor.self, forKey: .investor)
		enterprise = try values.decodeIfPresent(String.self, forKey: .enterprise)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
	}

}
