//
//  Enterprise_type.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation
struct Enterprise_type : Codable {
	let id : Int?
	let enterprise_type_name : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case enterprise_type_name = "enterprise_type_name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		enterprise_type_name = try values.decodeIfPresent(String.self, forKey: .enterprise_type_name)
	}

}
