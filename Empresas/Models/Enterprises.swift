//
//  Enterprises.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation
struct Enterprises : Codable {
	let enterprises : [Enterprise]?

	enum CodingKeys: String, CodingKey {

		case enterprises = "enterprises"

	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		enterprises = try values.decodeIfPresent([Enterprise].self, forKey: .enterprises)
	}

}
