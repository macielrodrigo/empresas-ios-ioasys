//
//  Headers.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

public class Headers {
    
    public var accessToken : String?
    public var client : String?
    public var expiry : String?
    public var uid : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Headers]
    {
        var models:[Headers] = []
        for item in array
        {
            models.append(Headers(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    
    required public init?(dictionary: NSDictionary) {
        
        accessToken = dictionary["access-token"] as? String
        client = dictionary["client"] as? String
        expiry = dictionary["expiry"] as? String
        uid = dictionary["uid"] as? String
    }
    
    
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.accessToken, forKey: "access-token")
        dictionary.setValue(self.client, forKey: "client")
        dictionary.setValue(self.expiry, forKey: "expiry")
        dictionary.setValue(self.uid, forKey: "uid")
        
        return dictionary
    }
    
    
    
    
}
