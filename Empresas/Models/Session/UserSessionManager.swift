//
//  UserSessionManager.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation


class UserSessionManager {
    
    static let sharedInstance = UserSessionManager()
    
    var userSession: UserSession? {
        set {
            if let valueToSave = newValue {
                UserDefaults.saveObjectWith(key: .headers, object: valueToSave)
            } else
            {
                removeSessionHeaders(key: .headers)
            }
        }
        get {
            let userSession = UserDefaults.getObjectWith(key: .headers, type: UserSession.self)
            
            return userSession
        }
    }
    
    
    func createUserSession(accessToken: String?, uid: String?, client: String?,  expiry: String?){
        
        if let uid = uid, let client = client, let accessToken = accessToken, let expiry = expiry {
            
            userSession = UserSession(accessToken: accessToken, uid: uid, client: client, expiry: expiry)
        }
    }
    
    func removeSessionHeaders(key: keysObjects) {
        
        UserDefaults.removeObjectWith(key: key)
    }
    
    func credentialsHeaders() -> [String: String] {
        
        if let uid = userSession?.uid, let client = userSession?.client, let accessToken = userSession?.accessToken {
            
            return ["uid": uid, "access-Token": accessToken, "client": client]
            
        }
        return ["":""]
    }
}

enum keysObjects: String {
    
    case headers = "headers"
    case login = "current_data"
    
}
