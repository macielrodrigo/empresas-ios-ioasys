//
//  UserSession.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

class UserSession: NSObject, NSCoding {
    
 
    private(set) var uid: String
    private(set) var client: String
    private(set) var accessToken: String
    private(set) var expiry: String
    
    init(accessToken: String, uid: String, client: String, expiry: String) {
        self.uid = uid
        self.client = client
        self.accessToken = accessToken
        self.expiry = expiry
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let uid = aDecoder.decodeObject(forKey: "uid") as? String,
            let client = aDecoder.decodeObject(forKey: "client") as? String,
            let accessToken = aDecoder.decodeObject(forKey: "accessToken") as? String,
            let expiry = aDecoder.decodeObject(forKey: "expiry") as? String else {
                return nil
        }
        
        self.init(accessToken: accessToken, uid: uid, client: client, expiry: expiry)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(uid, forKey: "uid")
        aCoder.encode(client, forKey: "client")
        aCoder.encode(accessToken, forKey: "accessToken")
        aCoder.encode(expiry, forKey: "expiry")
    }
    
}
