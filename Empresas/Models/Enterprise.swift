//
//  Enterprise.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

struct Enterprise : Codable {
    let id : Int?
    let email_enterprise : String?
    let facebook : String?
    let twitter : String?
    let linkedin : String?
    let phone : String?
    let own_enterprise : Bool?
    let enterprise_name : String?
    let photo : String?
    let description : String?
    let city : String?
    let country : String?
    let value : Int?
    let share_price : Int?
    let enterprise_type : Enterprise_type?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case email_enterprise = "email_enterprise"
        case facebook = "facebook"
        case twitter = "twitter"
        case linkedin = "linkedin"
        case phone = "phone"
        case own_enterprise = "own_enterprise"
        case enterprise_name = "enterprise_name"
        case photo = "photo"
        case description = "description"
        case city = "city"
        case country = "country"
        case value = "value"
        case share_price = "share_price"
        case enterprise_type = "enterprise_type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        email_enterprise = try values.decodeIfPresent(String.self, forKey: .email_enterprise)
        facebook = try values.decodeIfPresent(String.self, forKey: .facebook)
        twitter = try values.decodeIfPresent(String.self, forKey: .twitter)
        linkedin = try values.decodeIfPresent(String.self, forKey: .linkedin)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        own_enterprise = try values.decodeIfPresent(Bool.self, forKey: .own_enterprise)
        enterprise_name = try values.decodeIfPresent(String.self, forKey: .enterprise_name)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        value = try values.decodeIfPresent(Int.self, forKey: .value)
        share_price = try values.decodeIfPresent(Int.self, forKey: .share_price)
        enterprise_type = try values.decodeIfPresent(Enterprise_type.self, forKey: .enterprise_type)
    }
}
