//
//  Investor.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation
struct Investor : Codable {
	let id : Int?
	let investor_name : String?
	let email : String?
	let city : String?
	let country : String?
	let balance : Int?
	let photo : String?
	let portfolio : Portfolio?
	let portfolio_value : Int?
	let first_access : Bool?
	let super_angel : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case investor_name = "investor_name"
		case email = "email"
		case city = "city"
		case country = "country"
		case balance = "balance"
		case photo = "photo"
		case portfolio = "portfolio"
		case portfolio_value = "portfolio_value"
		case first_access = "first_access"
		case super_angel = "super_angel"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		investor_name = try values.decodeIfPresent(String.self, forKey: .investor_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		country = try values.decodeIfPresent(String.self, forKey: .country)
		balance = try values.decodeIfPresent(Int.self, forKey: .balance)
		photo = try values.decodeIfPresent(String.self, forKey: .photo)
		portfolio = try values.decodeIfPresent(Portfolio.self, forKey: .portfolio)
		portfolio_value = try values.decodeIfPresent(Int.self, forKey: .portfolio_value)
		first_access = try values.decodeIfPresent(Bool.self, forKey: .first_access)
		super_angel = try values.decodeIfPresent(Bool.self, forKey: .super_angel)
	}

}
