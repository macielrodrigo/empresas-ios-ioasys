//
//  LoginView.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

@IBDesignable
class LoginView: UIView {
    
    @IBInspectable lazy var logoImageView: UIImageView = {
        let i = UIImageView()
        i.backgroundColor = UIColor.clear
        i.image = UIImage(named: "img_logo_colorfull")
        i.contentMode = .scaleAspectFit
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    lazy var welcomeLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        l.textAlignment = .center
        l.textColor = UIColor(named: "charcoal_grey")
        l.text = "BEM-VINDO AO EMPRESAS"
        
        return l
    }()
    
    lazy var descriptionLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        l.textAlignment = .center
        l.textColor = UIColor(named: "charcoal_grey")
        l.text = "Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan." 
        
        return l
    }()
    
    @IBInspectable lazy var emailTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "E-mail"
        t.keyboardType = .emailAddress
        t.clearButtonMode = .whileEditing
        t.autocorrectionType = .no
        t.autocapitalizationType = .none
        t.borderStyle = .none
        t.backgroundColor = .clear
        t.textColor = UIColor(named: "charcoal_grey")
        t.translatesAutoresizingMaskIntoConstraints = false
        return t
    }()
    
    @IBInspectable lazy var passwordTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "Senha"
        t.keyboardType = .default
        t.isSecureTextEntry = true
        t.clearButtonMode = .whileEditing
        t.backgroundColor = .clear
        t.borderStyle = .none
        t.textColor = UIColor(named: "charcoal_grey")
        t.translatesAutoresizingMaskIntoConstraints = false
        
        return t
    }()
    
    
    @IBInspectable lazy var enterButton: UIButton = {
        let b = UIButton(type: .system)
        b.setTitle("ENTRAR", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        b.backgroundColor = UIColor(named: "greeny_blue")
        b.layer.cornerRadius = 6
        b.layer.masksToBounds = true
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setBackgroundImage(UIImage.fromColor(color: UIColor(white: 0.88, alpha: 0.46)), for: .disabled)
        return b
    }()
    
    @IBInspectable lazy var componentsView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    @IBInspectable lazy var emailView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    @IBInspectable lazy var passwordView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    @IBInspectable lazy var emailSeparetorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(named: "charcoal_grey")
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    @IBInspectable lazy var passwordSeparetorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(named: "charcoal_grey")
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    @IBInspectable lazy var emailIconImageView: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "ic_email")
        i.contentMode = .scaleAspectFit
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    @IBInspectable lazy var passwordIconImageView: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "ic_password")
        i.contentMode = .scaleAspectFit
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented - LoginView")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
}

extension LoginView {
    
    func setupConstraints() {
        
        self.addSubview(self.logoImageView)
        self.addSubview(self.welcomeLabel)
        self.addSubview(self.descriptionLabel)
        self.addSubview(self.componentsView)
        self.addSubview(self.enterButton)
        
        let marginsView = self.layoutMarginsGuide
        
        self.logoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 50.0).isActive = true
        self.logoImageView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        self.logoImageView.widthAnchor.constraint(equalToConstant: 164).isActive = true
        self.logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        self.welcomeLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        self.welcomeLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        self.welcomeLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 40).isActive = true
        
        self.descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        self.descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        self.descriptionLabel.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 18).isActive = true
        
        
        self.componentsView.addSubview(self.emailView)
        self.componentsView.addSubview(self.passwordView)
        
        
        let marginsComponentsView = self.componentsView.layoutMarginsGuide
        
        self.componentsView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        self.componentsView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        //self.componentsView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 40).isActive = true
        //self.componentsView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        self.componentsView.centerXAnchor.constraint(equalTo: marginsView.centerXAnchor).isActive = true
        self.componentsView.centerYAnchor.constraint(equalTo: marginsView.centerYAnchor).isActive = true
        self.componentsView.heightAnchor.constraint(equalToConstant: 110).isActive = true
        
        self.emailView.leadingAnchor.constraint(equalTo: marginsComponentsView.leadingAnchor).isActive = true
        self.emailView.trailingAnchor.constraint(equalTo: marginsComponentsView.trailingAnchor).isActive = true
        self.emailView.topAnchor.constraint(equalTo: marginsComponentsView.topAnchor).isActive = true
        self.emailView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        self.passwordView.topAnchor.constraint(equalTo: emailView.bottomAnchor, constant: 10).isActive = true
        self.passwordView.leadingAnchor.constraint(equalTo: marginsComponentsView.leadingAnchor).isActive = true
        self.passwordView.trailingAnchor.constraint(equalTo: marginsComponentsView.trailingAnchor).isActive = true
        self.passwordView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        //        //textField
        //
        self.emailView.addSubview(self.emailTextField)
        self.emailView.addSubview(self.emailIconImageView)
        self.emailView.addSubview(self.emailSeparetorView)
        self.passwordView.addSubview(self.passwordTextField)
        self.passwordView.addSubview(self.passwordIconImageView)
        self.passwordView.addSubview(self.passwordSeparetorView)
        //
        //         //icon textfield email
        self.emailIconImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        self.emailIconImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        self.emailIconImageView.leadingAnchor.constraint(equalTo: self.emailView.leadingAnchor).isActive = true
       // self.emailIconImageView.topAnchor.constraint(equalTo: self.emailView.topAnchor).isActive = true
        self.emailIconImageView.centerYAnchor.constraint(equalTo: self.emailView.centerYAnchor).isActive = true
        
        self.emailTextField.topAnchor.constraint(equalTo: self.emailView.topAnchor).isActive = true
        self.emailTextField.leadingAnchor.constraint(equalTo: self.emailIconImageView.trailingAnchor, constant: 4).isActive = true
        self.emailTextField.trailingAnchor.constraint(equalTo: self.emailView.trailingAnchor).isActive = true
        self.emailTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.emailSeparetorView.topAnchor.constraint(equalTo: self.emailTextField.bottomAnchor).isActive = true
        self.emailSeparetorView.leadingAnchor.constraint(equalTo: self.emailView.leadingAnchor).isActive = true
        self.emailSeparetorView.trailingAnchor.constraint(equalTo: self.emailView.trailingAnchor).isActive = true
        self.emailSeparetorView.heightAnchor.constraint(equalToConstant: 1.8).isActive = true
        ////
        ////         //icon textfield password
        self.passwordIconImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        self.passwordIconImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        self.passwordIconImageView.leadingAnchor.constraint(equalTo: self.passwordView.leadingAnchor).isActive = true
        //self.passwordIconImageView.topAnchor.constraint(equalTo: self.passwordView.topAnchor).isActive = true
        self.passwordIconImageView.centerYAnchor.constraint(equalTo: self.passwordView.centerYAnchor).isActive = true
        //
        self.passwordTextField.topAnchor.constraint(equalTo: self.passwordView.topAnchor).isActive = true
        self.passwordTextField.leadingAnchor.constraint(equalTo: self.passwordIconImageView.trailingAnchor, constant: 4).isActive = true
        self.passwordTextField.trailingAnchor.constraint(equalTo: self.passwordView.trailingAnchor).isActive = true
        self.passwordTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //
        self.passwordSeparetorView.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor).isActive = true
        self.passwordSeparetorView.leadingAnchor.constraint(equalTo: self.passwordView.leadingAnchor).isActive = true
        self.passwordSeparetorView.trailingAnchor.constraint(equalTo: self.passwordView.trailingAnchor).isActive = true
        self.passwordSeparetorView.heightAnchor.constraint(equalToConstant: 1.8).isActive = true
        
        
        //enter button
        self.enterButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
        self.enterButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
        self.enterButton.heightAnchor.constraint(equalToConstant: 52.8).isActive = true
        self.enterButton.bottomAnchor.constraint(equalTo: marginsView.bottomAnchor, constant: -5).isActive = true
        
    }
    
}
