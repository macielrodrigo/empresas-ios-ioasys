//
//  EnterpriseViewCell.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 17/01/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

class EnterpriseViewCell: UITableViewCell {

    @IBOutlet weak var nameEnterprise: UILabel!
    @IBOutlet weak var typeEnterprise: UILabel!
    @IBOutlet weak var localizacaoEnterprise: UILabel!
    @IBOutlet weak var photo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
