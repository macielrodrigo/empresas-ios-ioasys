//
//  ApiRouter.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation
import UIKit

enum ApiRouter {
    
    static let baseUrlString = Constants.baseUrl
    static var OAuthToken: String?
    static let apiVersion = Constants.apiVersion
    
    case login(Login)
    case getEnteprises
    case getEnterpriseDetails(id: Int)
    
    
    private enum HTTPMethod {
        case get
        case post
        case put
        case delete
        
        var value: String {
            switch self {
            case .get: return "GET"
            case .post: return "POST"
            case .put: return "PUT"
            case .delete: return "DELETE"
            }
        }
    }
    
    private var method: HTTPMethod {
        switch self {
        case .login: return .post
        case .getEnteprises: return .get
        case .getEnterpriseDetails: return .get
        }
    }

    
    var path: String {
        switch self {
        case .login:
            return "users/auth/sign_in"
        case .getEnteprises:
            return "enterprises"
        case .getEnterpriseDetails(let id):
            return "enterprises/\(id)"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let urlString = "\(ApiRouter.baseUrlString)\(ApiRouter.apiVersion)\(path)"
        
        guard let url = URL(string: urlString) else { throw ErrorType.parseUrlFail}
        
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        urlRequest.httpMethod = method.value
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        switch self {
        case .login(let login):
            
            let encoder = JSONEncoder()
            let data = try! encoder.encode(login)
            urlRequest.httpBody = data
            
            return urlRequest
        case .getEnteprises:
            urlRequest.allHTTPHeaderFields = UserSessionManager.sharedInstance.credentialsHeaders()
            return urlRequest
        case .getEnterpriseDetails:
            return urlRequest
        }
        
    }

}
