//
//  ListCompanyViewController.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 17/01/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

class ListEnterpriseViewController: BaseViewController, LoadingView {
    
    private var enterpriseTableView: EnterpriseTableViewController!
    
    private lazy var listCompanyViewModel = ListEnterpriseViewModel()
    private var refreshControl: UIRefreshControl!

    
    init(appRouter: AppRouter? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.appRouter = appRouter
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
         self.view.backgroundColor = UIColor(named: "beige")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = UIImageView(image: UIImage(named: "img_logo_white"))
        self.setupCompanyTableView()
        self.setupRefreshControl()
        self.listCompanyViewModel.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadListEnterprise()
    }
    
    func loadListEnterprise() {
        showLoadingViewWithLabel(text: "Carregando Empresas")
        self.listCompanyViewModel.listEnterprise()
    }
}

extension ListEnterpriseViewController {
    
    func setupCompanyTableView(){
        
        self.enterpriseTableView = EnterpriseTableViewController(appRouter: self.appRouter)
        self.addChild(self.enterpriseTableView)
        self.enterpriseTableView.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.enterpriseTableView.view)
        setupConstraintsCompanyView()
        self.enterpriseTableView.didMove(toParent: self)
        
    }
    
    func setupConstraintsCompanyView() {
        
        self.enterpriseTableView.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.enterpriseTableView.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.enterpriseTableView.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.enterpriseTableView.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    func setupRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: UIControl.Event.valueChanged)
        self.enterpriseTableView.tableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender:AnyObject) {
        // Code to refresh table view
        self.loadListEnterprise()
    }
    
}

extension ListEnterpriseViewController: ListEnterpriseViewControllerDelegate, MessagePresenter {
    
    func getListEnterprise(enterprises: Enterprises) {
        
        var listEnterprises: [Enterprise] = []
        
        if let enterprises = enterprises.enterprises {
            listEnterprises = enterprises
        }
        self.enterpriseTableView.listEnterprises = listEnterprises
        self.refreshControl.endRefreshing()
        hideLoadingView()
    }
    
    func failListEnterprise(error: Error) {
        self.refreshControl.endRefreshing()
        hideLoadingView()
        showErrorMessage(error: error.localizedDescription)
    }
    
}
