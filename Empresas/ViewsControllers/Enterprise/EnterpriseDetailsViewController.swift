//
//  CompanyDetailsViewController.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

class EnterpriseDetailsViewController: UIViewController {
    
    private var enterpriseDetailsView: EnterpriseDetailsView!
    
    private var enterprise: Enterprise?
    
    init(enterprise: Enterprise? = nil) {
        self.enterprise = enterprise
        super.init(nibName: nil, bundle: nil)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = .white
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.enterpriseDetailsView = EnterpriseDetailsView(frame: UIScreen.main.bounds)
        self.view.addSubview(self.enterpriseDetailsView)
        
        self.loadData()
 
    }

}

extension EnterpriseDetailsViewController {
    
    func loadData() {
        
        if let nameEnterprise = self.enterprise?.enterprise_name {
            self.navigationItem.title = nameEnterprise
        }
        if let description = self.enterprise?.description {
            self.enterpriseDetailsView.textLabel.text = description
        }
        
        if let urlPhoto = self.enterprise?.photo {
            self.enterpriseDetailsView.imageView.load(url: URL(string: urlPhoto)!)
        }
    }
}
