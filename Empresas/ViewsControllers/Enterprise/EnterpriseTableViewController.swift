//
//  CompanyTableViewController.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 17/01/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

class EnterpriseTableViewController: UITableViewController {
    
    var appRouter: AppRouter?
    
    var listEnterprises: [Enterprise] = []{
        didSet {
            self.tableView.reloadData()
        }
    }
    
    init(appRouter: AppRouter? = nil) {
        
        self.appRouter = appRouter
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        self.tableView.register(UINib(nibName: "EnterpriseViewCell", bundle: nil), forCellReuseIdentifier: "enterpriseViewCell")
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.listEnterprises.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "enterpriseViewCell", for: indexPath) as? EnterpriseViewCell else {
            fatalError("Could not dequeue cell with identifier: companyViewCell")
        }
        
        let enterprise = self.listEnterprises[indexPath.row]
        cell.nameEnterprise.text = enterprise.enterprise_name
        cell.typeEnterprise.text = enterprise.enterprise_type?.enterprise_type_name
        
            if let urlPhoto = enterprise.photo {
                cell.photo.load(url: URL(string: urlPhoto)!)
            } else {
                cell.photo.image = UIImage(named: "notfound.png")
            }
        
        
        if let city = enterprise.city, let country = enterprise.country {
            cell.localizacaoEnterprise.text = "\(city) - \(country)"
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let enterprise = self.listEnterprises[indexPath.row]
        self.appRouter?.didSelectEnterprise(tableView: tableView, indexPath: indexPath, enterprise: enterprise)
    }
    
}
