//
//  LoginViewController.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    private var loginView: LoginView!
    private lazy var  loginViewModel = LoginViewModel()
    private var emailBool: Bool = false
    private var passwordBool: Bool = false
    private var currentPassword: String = ""
    var appRouter: AppRouter?
    
    init(appRouter: AppRouter? = nil) {
        
        self.appRouter = appRouter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = UIColor(named: "beige")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loginView = LoginView(frame: UIScreen.main.bounds)
        self.view.addSubview(self.loginView)
        loginViewModel.delegate = self
        
        setCredentialsInTextField()
        isEnabledEnterButton()
        
        configureActions()
        configureTapGesture()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.loginView.frame = self.view.bounds
    }
}

extension LoginViewController {
    
    func setCredentialsInTextField() {
        loginViewModel.setCredentialsInTextField(with: self.loginView.emailTextField, password: self.loginView.passwordTextField)
        self.emailBool = loginViewModel.emailTextFieldDidChange(self.loginView.emailTextField)
        self.passwordBool = loginViewModel.passwordTextFieldDidChange(self.loginView.passwordTextField)
    }
    
    func configureActions() {
        self.loginView.emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.loginView.passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.loginView.enterButton.addTarget(self, action: #selector(self.enterButtonTapped(_:)), for: .touchUpInside)
    }
    
    func configureTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func isEnabledEnterButton() {
        
        if self.emailBool && self.passwordBool {
            self.loginView.enterButton.isEnabled = true
            self.loginView.enterButton.alpha = 1.0
        } else {
            self.loginView.enterButton.isEnabled = false
            self.loginView.enterButton.alpha = 0.5
        }
    }
}

//MARK: - textFieldDidChange
extension LoginViewController {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField == self.loginView.emailTextField {
            self.emailBool = self.loginViewModel.emailTextFieldDidChange(textField)
            isEnabledEnterButton()
            
        } else if textField == self.loginView.passwordTextField {
            self.passwordBool = self.loginViewModel.passwordTextFieldDidChange(textField)
            isEnabledEnterButton()
        }
    }
}

extension LoginViewController: LoadingView {
    
    @objc func enterButtonTapped(_ sender: AnyObject) {
        
        self.dismissKeyboard()
        self.showLoadingViewWithLabel(text: "Logando")
        loginViewModel.sendLogin(with: self.loginView.emailTextField.text, passwordTextField: self.loginView.passwordTextField.text)
        
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}

extension LoginViewController: LoginViewControllerDelegate, MessagePresenter {
    
    func getLogin(user: User?, error: Error?) {
        
        self.hideLoadingView()
        if let error = error {
            showErrorMessage(error: error.localizedDescription)
        } else
        {
            self.appRouter?.showListEnterpriseViewController()
        }
    }
}
