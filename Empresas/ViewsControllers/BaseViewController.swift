//
//  BaseViewController.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var appRouter: AppRouter?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(named: "beige")
        self.title = "Empresas"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let button = UIBarButtonItem(title: "Sair", style: .plain, target: self, action: #selector(BaseViewController.logout))
        self.navigationItem.rightBarButtonItem  = button
    }
    
    @objc func logout() {
        
        self.appRouter?.logout()
    }

}
