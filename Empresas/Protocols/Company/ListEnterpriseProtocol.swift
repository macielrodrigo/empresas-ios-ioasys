//
//  ListEnterpriseProtocol.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

protocol ListEnterpriseViewControllerDelegate {
    
    func getListEnterprise(enterprises: Enterprises)
    func failListEnterprise(error: Error)
}
