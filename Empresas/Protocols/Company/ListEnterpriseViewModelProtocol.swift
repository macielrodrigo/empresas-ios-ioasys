//
//  ListEnterpriseViewModelProtocol.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

protocol ListEnterpriseViewModelProtocol {
    
    func listEnterprise()
}
