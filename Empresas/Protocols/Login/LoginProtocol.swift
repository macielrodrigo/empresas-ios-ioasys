//
//  LoginProtocol.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation


protocol LoginViewControllerDelegate {
    func getLogin(user: User?, error: Error?)
    func loginViewController(_ controller: LoginViewController)
}

extension LoginViewControllerDelegate {
    
    func loginViewController(_ controller: LoginViewController) {
        
    }
}
