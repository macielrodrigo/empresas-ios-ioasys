//
//  LoginViewModelProtocol.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

protocol LoginViewModelProtocol {
    
    func sendLogin(with emailTextField: String?, passwordTextField: String?)
    func setCredentialsInTextField(with email: UITextField?, password: UITextField? ) 
    func isValidEmail(_ emailAddress: String?) -> Bool
}
