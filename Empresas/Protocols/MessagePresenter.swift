//
//  MessagePresenter.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

protocol MessagePresenter {
    
    func showErrorMessage(error: String?)
    func showMessage(message: String?, title: String?)
    
}
