//
//  Result.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(data: T)
    case failure(error: Error)
}
