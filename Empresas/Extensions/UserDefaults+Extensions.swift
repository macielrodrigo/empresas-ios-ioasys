//
//  UserDefaults+Extensions.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation


extension UserDefaults {
    
    static func saveObjectWith(key: keysObjects, object: NSObject?) {
        
        let data: Data?
        if let object = object {
            data = try? NSKeyedArchiver.archivedData(withRootObject: object, requiringSecureCoding: false) as Data?
        } else {
            data = nil
        }
        
        UserDefaults.standard.set(data, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func getObjectWith<T: NSObject>(key: keysObjects, type: T.Type) -> T? {
        
        guard let data = UserDefaults.standard.object(forKey:  key.rawValue) as? Data,
            let object = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as Data) as? T else {
                return nil
        }
        return object
    }
    
    static func removeObjectWith(key: keysObjects) {
        
        UserDefaults.standard.removeObject(forKey: key.rawValue)
        
    }
    
}
