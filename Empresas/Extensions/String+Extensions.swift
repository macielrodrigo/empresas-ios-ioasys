//
//  String+Extensions.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

extension String {
    
    func isValidEmailAddress() -> Bool {
        
        let email = self
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = email as NSString
            let results = regex.matches(in: email, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            debugPrint("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
}
