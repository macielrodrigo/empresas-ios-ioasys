//
//  LoadingView+Extensions.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 11/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

extension LoadingView where Self: UIViewController {
    private var tag: Int {
        return 684
    }
    
    func hideLoadingView() {
        //DispatchQueue.main.async {
            self.view.superview?.viewWithTag(self.tag)?.removeFromSuperview()
        //}
    }
    
    func showLoadingViewWithLabel(text: String) {
        let loadingView = ActivityView(frame:  self.view.frame)
        loadingView.tag = tag
        loadingView.textLabel.text = text
        view.superview?.addSubview(loadingView)
    }
    
    func showLoadingView() {
        let loadingView = UIView(frame:  self.view.frame) //
        loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        loadingView.addSubview(indicatorView())
        loadingView.tag = tag
        view.superview?.addSubview(loadingView)
    }
    
    func indicatorView() -> UIActivityIndicatorView {
        let indicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        indicatorView.center = CGPoint(x: view.center.x, y: view.center.y)
        indicatorView.startAnimating()
        return indicatorView
    }
}
