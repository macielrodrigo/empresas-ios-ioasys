//
//  MessagePresenter+Extensions.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

extension MessagePresenter where Self: UIViewController {
    
    func showErrorMessage(error: String?) {
        showMessage(message: error, title: "Atenção")
    }
    
    func showMessage(message: String?, title: String? = nil) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
