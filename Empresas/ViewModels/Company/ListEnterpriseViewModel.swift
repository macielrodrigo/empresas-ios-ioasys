//
//  ListCompanyViewModel.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import Foundation

class ListEnterpriseViewModel: ListEnterpriseViewModelProtocol {
    
    var delegate: ListEnterpriseViewControllerDelegate?
   
    func listEnterprise() {
        
        ApiManager.shared.request(router: ApiRouter.getEnteprises) { (result: Result<Enterprises>) in
            
            switch result {
            case .success(let enterprises):
                self.delegate?.getListEnterprise(enterprises: enterprises)
            case .failure(let error):
                self.delegate?.failListEnterprise(error: error)
            }
        }
        
    }
}
