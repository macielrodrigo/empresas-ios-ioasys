//
//  LoginViewModel.swift
//  Empresas
//
//  Created by Rodrigo Maciel on 10/02/20.
//  Copyright © 2020 Rodrigo Maciel. All rights reserved.
//

import UIKit

class LoginViewModel: LoginViewModelProtocol {
    
    var delegate: LoginViewControllerDelegate?
    
    func emailTextFieldDidChange(_ email: UITextField) -> Bool {
        
        return isValidEmail(email.text)
    }
    
    func passwordTextFieldDidChange(_ password: UITextField) -> Bool {
        
        if let password = password.text {
            
            return password.count >= 4
        }
        
        return false
        
    }
    
    func isValidEmail(_ emailAddress: String?) -> Bool {

        if let email = emailAddress {
            
            return email.isValidEmailAddress()
        }
        return false
    }
    
    func setCredentialsInTextField(with email: UITextField?, password: UITextField? ) {
        
        let login = LoginManager.sharedInstance.login
        
        email?.text = login?.email
        password?.text = login?.password
        
    }
    
    func sendLogin(with emailTextField: String?, passwordTextField: String?) {
          
          guard let email = emailTextField else {return}
          guard let password = passwordTextField else {return}
      
          let login = Login(email: email, password: password)
          LoginManager.sharedInstance.saveLogin(email: email, password: password)
          
          ApiManager.shared.request(router: ApiRouter.login(login)) { (result: Result<User>) in
              
              switch result {
              case .success(let user):
                  self.delegate?.getLogin(user: user, error: nil)
              case .failure(let error):
                  self.delegate?.getLogin(user: nil, error: error)
              }
          }
      }
}
